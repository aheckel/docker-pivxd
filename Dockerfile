FROM debian:buster-slim
LABEL maintainer="Alexander Heckel <alexander.heckel@icloud.com>"

ARG DEBIAN_FRONTEND=noninteractive

ARG PIVX_BASE=/opt/pivx

ARG PIVX_USER_NAME=pivx
ARG PIVX_USER_UID=1000

ARG PIVX_VERSION

ARG PIVX_DOWNLOAD_BASE="https://github.com/pivx-project/pivx/releases/download/v${PIVX_VERSION}"

ENV PATH="${PATH}:/opt/pivx/bin"

RUN apt-get -qq update && \
    apt-get install -yq --no-install-recommends curl ca-certificates && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN mkdir -p "${PIVX_BASE}" && \
    curl -sL "${PIVX_DOWNLOAD_BASE}/pivx-${PIVX_VERSION}-$(uname -m)-linux-gnu.tar.gz" | tar xvz --strip-components 1 -C "${PIVX_BASE}"

RUN adduser --uid "${PIVX_USER_UID}" --system "${PIVX_USER_NAME}" && \
    cp -r "${PIVX_BASE}"/share/pivx /home/"${PIVX_USER_NAME}"/.pivx-params/ && \
    chown -R "${PIVX_USER_NAME}" /home/"${PIVX_USER_NAME}"

USER "${PIVX_USER_NAME}"
WORKDIR /home/"${PIVX_USER_NAME}"

EXPOSE 51472
VOLUME ["/home/pivx/.pivx"]
ENTRYPOINT ["/opt/pivx/bin/pivxd"]

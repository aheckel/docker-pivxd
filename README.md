# Multi Arch Build

```bash
export PIVX_VERSION=5.0.1
docker buildx build --build-arg PIVX_VERSION --platform linux/arm64,linux/amd64 -t aheckel/pivxd:"${PIVX_VERSION}" --push .
```
